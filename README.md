# Guide d'utilisation #
package main: contient le main qui simule la commande egrep
### Egrep.java : affiche toutes les lignes du fichier contenant la regex(ERE) donnée en argument ###
2 arguments attendus:

* Un String correspondant à la regex
* Un String correspondant au chemin vers le fichier à tester

### package tests: contient les tests de chaque partie de la methode Aho-Ullman ###

* testRegExTreeToNfa.java: conversions des RegExTree en automate finis non déterministes(NFA)
* testNfatoDfa: tests de déterminisation des automate finis non déterministes
* testRecognizeString: tests unitaires (Junit5) de reconnaissance de mot dans les automates finis déterminisites (DFA)


### package utilitaires: contient le corps des méthodes qui implémentent la méthode d'Aho-Ullman ###

* RegExTree.java: Nfa toNfa(RegExTree) :transformation du regExTree en NFA en utilisant l'algorithme d'Aho-Ullman
* Nfa.java: Dfa toDfa(Nfa n) : transformation du NFA en DFA en utilisant la méthode des sous-ensembles
* Dfa.java: DFA utilisé pour la reconnaissance des regex dans Egrep.java(package main)
