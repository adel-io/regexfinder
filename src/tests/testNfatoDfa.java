package tests;

import utilitaires.Dfa;
import utilitaires.Nfa;
import utilitaires.RegEx;
import utilitaires.RegExTree;

public class testNfatoDfa {
	public static void  Exampleconcat() throws Exception{
		String r="Sargon";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();

		
	}
	public static void  Examplealt() throws Exception{
		String r="Sargon(i|.)";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();

		
	}
	
	public static void  Exampleprojet() throws Exception{
		String r="S(a|g|r)+on";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();

		
	}
	
	public static void  ExampleAhoUllman() throws Exception{
		String r="a|bc*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();

		
	}
	

	public static void  ExampleKleeneStar() throws Exception{
		String r="a|(bc)*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
			
	}
		
	
	
	public static void  ExampleRapport1() throws Exception{
		String r="a|g|r";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
	}
	
	public static void  ExampleRapport2() throws Exception{
		String r="(a|g|r)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
	}
	
	public static void main(String[] args) {
		long startTime,stopTime;
		try {
			startTime=System.nanoTime();
			Exampleconcat();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			

			startTime=System.nanoTime();
			Examplealt();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			Exampleprojet();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleAhoUllman();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleKleeneStar();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleRapport1();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleRapport2();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
