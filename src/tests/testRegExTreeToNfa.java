package tests;

import utilitaires.Nfa;
import utilitaires.RegEx;
import utilitaires.RegExTree;

public class testRegExTreeToNfa {

	public static void  ExampleAhoUllman() throws Exception{
		String r="a|bc*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	
	public static void  Exampleconcat() throws Exception{
		String r="Sargon";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	public static void  Examplealt() throws Exception{
		String r="Sargon(i|.)";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	
	public static void  ExampleKleeneStar() throws Exception{
		String r="a|(bc)*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	
	
	public static void  Exampleprojet() throws Exception{
		String r="S(a|g|r)+on";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	
	public static void  testRapport1() throws Exception{
		String r="(a|g|r)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	

	public static void  testRapport2() throws Exception{
		String r="a|g|r";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		n.resetStates();
		
	}
	
	
	
	public static void main(String[] args) {
		long startTime,stopTime;
		try {
			startTime=System.nanoTime();
			ExampleAhoUllman();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			
			
			startTime=System.nanoTime();
			Exampleconcat();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			
			
			startTime=System.nanoTime();
			Examplealt();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			
			startTime=System.nanoTime();
			ExampleKleeneStar();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			
			startTime=System.nanoTime();
			Exampleprojet();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			
			startTime=System.nanoTime();
			testRapport1();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			startTime=System.nanoTime();
			
			testRapport2();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			
			
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
