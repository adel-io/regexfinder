package tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import main.Egrep;
import utilitaires.Dfa;
import utilitaires.Nfa;
import utilitaires.RegEx;
import utilitaires.RegExTree;

public class testRecognizeString {

	@Test
	void testsExampleAhoUllman() throws Exception {
		String r = "a|bc*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= " + r);
		// System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		n.resetStates();
		System.out.println();

		// cas positifs
		assertTrue(Egrep.recognize("a", d));

		assertTrue(Egrep.recognize("b", d));
		assertTrue(Egrep.recognize("bc", d));
		assertTrue(Egrep.recognize("bcc", d));
		assertTrue(Egrep.recognize("bcccccccccccc", d));

		// cas n�gatifs
		assertFalse(Egrep.recognize("ab", d));
		assertFalse(Egrep.recognize("ac", d));
		assertFalse(Egrep.recognize("sargon", d));

	}

	@Test
	void testsExampleconcat() throws Exception {
		String r = "Sargon";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= " + r);
		System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		n.resetStates();
		System.out.println();

		// cas positifs
		assertTrue(Egrep.recognize("Sargon", d));

		// cas n�gatifs
		assertFalse(Egrep.recognize("Sargonides", d));
		assertFalse(Egrep.recognize("babylone", d));

	}
	@Test
	void  testExampleKleeneClosure() throws Exception{
		String r="a|(bc)*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
		System.out.println();
		
		// cas positifs
		assertTrue(Egrep.recognize("a", d));
		assertTrue(Egrep.recognize("bc", d));
		assertTrue(Egrep.recognize("bcbc", d));
		assertTrue(Egrep.recognize("bcbcbc", d));
		
		// cas n�gatifs
		assertFalse(Egrep.recognize("ab", d));
		assertFalse(Egrep.recognize("abc", d));
			
	}
	@Test
	void  testExampleKleeneClosure2() throws Exception{
		String r="(a|b)*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
		System.out.println();
		Egrep.recognize("", d);
		
		// cas positifs
		//assertTrue(Egrep.recognize("", d));
		assertTrue(Egrep.recognize("a", d));
		assertTrue(Egrep.recognize("b", d));
		
		assertTrue(Egrep.recognize("aaaa", d));
		
		assertTrue(Egrep.recognize("bbbb", d));
		
		assertTrue(Egrep.recognize("abababababa", d));
		
		// cas n�gatifs
		assertFalse(Egrep.recognize("abc", d));
		//assertFalse(Egrep.recognize("abc", d));
			
	}
	
	
	
	void  testExampleKleeneClosure3() throws Exception{
		String r="(a|b)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= "+r);
		System.out.println(n.toString());
		Dfa d=n.toDfa(n);
		n.resetStates();
		System.out.println();
		
		// cas positifs
		assertTrue(Egrep.recognize("a", d));
		assertTrue(Egrep.recognize("b", d));
		
		assertTrue(Egrep.recognize("aaaa", d));
		
		assertTrue(Egrep.recognize("bbbb", d));
		
		assertTrue(Egrep.recognize("abababababa", d));
		
		// cas n�gatifs
		assertFalse(Egrep.recognize("abc", d));
		//assertFalse(Egrep.recognize("abc", d));
			
	}
	
	@Test
	void testsExamplealt() throws Exception {
		String r = "Sargon(i|.)";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= " + r);
		// System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		n.resetStates();
		System.out.println();

		// cas positifs
		assertTrue(Egrep.recognize("Sargon.", d));
		assertTrue(Egrep.recognize("Sargoni", d));

		// cas n�gatifs
		assertFalse(Egrep.recognize("Sargonides", d));
		assertFalse(Egrep.recognize("babylone", d));

	}

	
	@Test
	void testsSimpleprojet() throws Exception {
		String r="(a|r|g)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= " + r);
		System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		n.resetStates();
		System.out.println();

		// cas positifs
		
		assertTrue(Egrep.recognize("arg", d));
		assertTrue(Egrep.recognize("argarg", d));
	


		// cas n�gatifs
		assertFalse(Egrep.recognize("", d));
		assertFalse(Egrep.recognize("argc", d));
		assertFalse(Egrep.recognize("babylone", d));

	}
	
	//KO
	/*
	@Test
	void testsExampleprojet() throws Exception {
		String r="S(a|r|g)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		Nfa n = ret.toNfa(ret);
		System.out.println("Regex= " + r);
		System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		n.resetStates();
		System.out.println();

		// cas positifs
		assertTrue(Egrep.recognize("Sargon", d));


		// cas n�gatifs
		assertFalse(Egrep.recognize("babylone", d));

	}*/
	


}
