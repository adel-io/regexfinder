package tests;

import utilitaires.RegEx;
import utilitaires.RegExTree;

public class testRegExToRegExTree {
	public static void  Exampleconcat() throws Exception{
		String r="Sargon";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	public static void  Examplealt() throws Exception{
		String r="Sargon(i|.)";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	public static void  Exampleprojet() throws Exception{
		String r="S(a|g|r)+on";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}

	
	public static void  ExampleAhoUllman() throws Exception{
		String r="a|bc*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	
	public static void  ExampleKleeneStar() throws Exception{
		String r="a|(bc)*";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	public static void  ExampleRapport1() throws Exception{
		String r="a|g|r";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	
	public static void  ExampleRapport2() throws Exception{
		String r="(a|g|r)+";
		RegEx regex = new RegEx(r);
		RegExTree ret = regex.parse();
		System.out.println("RegExTree de la regex "+ r+" :");
		System.out.println(ret.toString());
		//System.out.println("parcours postfixe: ");
		//System.out.println(ret.postOrder());
	}
	
	
	
	
	
	public static void main(String[] args) {
		long startTime,stopTime;
		try {
			startTime=System.nanoTime();
			Exampleconcat();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			

			startTime=System.nanoTime();
			Examplealt();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			Exampleprojet();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleAhoUllman();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleKleeneStar();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleRapport1();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			startTime=System.nanoTime();
			ExampleRapport2();
			stopTime = System.nanoTime();
			System.out.println("execution time (ns): "+(stopTime - startTime));
			System.out.println();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
