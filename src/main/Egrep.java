package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import utilitaires.Dfa;
import utilitaires.Nfa;
import utilitaires.RegEx;
import utilitaires.RegExTree;

public class Egrep {
	private static Scanner scanner;

	public static boolean recognize(String str, Dfa d) {
		int state = 0; // the current state
		int index = 0; // index of the current character
		char current; // the current character

		// continue while there are input characters
		// and the reject state has not been reached
		while (index < str.length() && state != d.getReject()) {
			current = str.charAt(index++);
			state = d.getTransition()[state][current];
		}

		// if the final state is reached with the last character, the string is accepted.
		if (index == str.length() && d.isFinal(state))
			return true;
		else
			return false;
	}
	
	//return true if the word contains a suffix that is recognizable by the dfa
	public static boolean findMatchingSuffixe(String mot, Dfa d) {
		int endcursor = 1;
		String suffixe;
		while (endcursor <= mot.length()) {
			suffixe = mot.substring(0, endcursor);
			if (recognize(suffixe, d))
				return true;
			endcursor++;
		}

		return false;
	}

	// print lines that contain suffixes that match with the regex
	public static void egrep(String regex, String filepath) throws Exception {
		boolean found=false;
		String line;
		String[] words;
		RegEx reg = new RegEx(regex);
		RegExTree ret = reg.parse();
		// System.out.println(" >> Tree result: " + ret.toString());
		// System.out.println(" >> bottomUp Order: " + ret.postOrder());
		Nfa n = ret.toNfa(ret);
		System.out.println(n.toString());
		Dfa d = n.toDfa(n);
		BufferedReader reader;		
		//int matchinglines=0;
		try {
			FileReader f=new FileReader(filepath);
			reader = new BufferedReader(f);
			line = reader.readLine();
			while (line != null) {
				words = line.split(" ");
				for (String word : words) {	
					if (findMatchingSuffixe(word, d)) {
						found=true;
						System.out.println(line);
						//matchinglines++;
						break;
					}

				}
				line = reader.readLine();
			}
			if(!found) System.out.println("regex "+ regex+ " not found in file "+ filepath);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("matching lines: "+matchinglines);
	}


	public static void main(String arg[]) {
		String regEx;
		String filepath;
		long startTime;
		long stopTime;

		System.out.println("Welcome to Egrep simulation");
		if (arg.length != 0) {
			regEx = arg[0];
		} else {
			scanner = new Scanner(System.in);
			System.out.print("  >> Please enter a regEx: ");
			regEx = scanner.next();
		}
		if (regEx.length() < 1)
			System.err.println("  >> ERROR: empty regEx.");
		
		scanner = new Scanner(System.in);
		System.out.print("  >> Please enter the file path: ");
		filepath = scanner.next();
		scanner.close();
		
		if (filepath.length() < 1)
			System.err.println("  >> ERROR: empty filepath.");
		try {
			startTime=System.nanoTime();
			egrep(regEx, filepath);
			stopTime = System.nanoTime();
			System.out.println(TimeUnit.NANOSECONDS.toMillis(stopTime - startTime));
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

}
