package utilitaires;

public class Dfa
{ 
	  private final int reject   = -1 ;  
	  final int asciiMin = 0 ;  
	  final int asciiMax = 255 ; 
	  
	  private int     numStates = 0 ;   //number of states
	  private int[][] transition ;      
	

	  private int     numFinal = 0;    	// number of final states
	  private int[]   finalStates ;     // array of final states
	  public Dfa()
	  {
	   
	    transition = new int[numStates][asciiMax+1] ;
	    
	    // Initialize the array of final states
	    finalStates = new int[ numFinal ] ;
	    
	    // initialize all states to reject
	    for ( int s=0; s<numStates; s++ )
	      for (int j=0; j<asciiMax; j++ )
	        transition[s][j] = getReject() ;
	  }
	  
	  public Dfa( int numState, int numFinal)
	  {
	    numStates = numState ;
	    transition = new int[numStates][asciiMax+1] ;
	    finalStates = new int[numFinal] ;
	    
	    // transition matrix init
	    for ( int s=0; s<numStates; s++ )
	      for (int j=0; j<asciiMax; j++ )
	        transition[s][j] = getReject() ;
	  }
	  public int[][] getTransition() {
			return transition;
		}
	  public int[] getfinalStates() {
		  return finalStates;
	  }
	  public void addTrans( int currentState, char input, int nextState )
	  {
	    if ( currentState>=0 && currentState<numStates && 
	         nextState   >=0 && nextState   <numStates && 
	         input>=asciiMin && input <=asciiMax )
	    {
	      transition[currentState][input] = nextState ;
	    }
	    else
	      System.out.println("Problem with the transition: " + 
	        currentState + " reading " + input + " -> " + nextState );
	  } 

	  public void addFinal( int state )
	  {
	    if ( numFinal > finalStates.length-1 ) 
	    {
	      System.out.println("No place for final state:" + state );
	      return;
	    }
	    
	    for(int i=0;i<finalStates.length;i++) {
	    	if(finalStates[i]==state) return;
	    }
	    finalStates[numFinal] = state;
	    numFinal++ ;
	  } 

	  /* search for a final state in the list of final states */  
	  public boolean isFinal( int state )
	  {
	    int j;
	    for ( j=0; j<numFinal; j++ )
	      if ( state==finalStates[j] ) return true; 
	    
	    return false;
	  }

	public int getReject() {
		return reject;
	}

}
