package utilitaires;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Nfa {
	private Integer startState;
	private Integer finalState;
	private Map<Integer, List<Transition>> trans;
	private static int start = 1;
	private static int accepting = 2;

	public Nfa() {
		this.startState = start;
		this.finalState = accepting;
		start += 3;
		accepting += 3;
		trans = new HashMap<Integer, List<Transition>>();
		if (!startState.equals(finalState))
			trans.put(finalState, new LinkedList<Transition>());
	}

	public Nfa(Integer startState, Integer finalState) {
		this.startState = startState;
		this.finalState = finalState;
		trans = new HashMap<Integer, List<Transition>>();
		if (!startState.equals(finalState))
			trans.put(finalState, new LinkedList<Transition>());
	}

	public Nfa(Integer startState, Integer finalState, Map<Integer, List<Transition>> tr) {
		this.startState = startState;
		this.finalState = finalState;
		trans = new HashMap<Integer, List<Transition>>(tr);
	}
	public Integer getFinalState() {
		return finalState;
	}

	public Integer getNumberStates() {
		return trans.size();
	}

	public Integer getStart() {
		return startState;
	}

	public Integer getFinal() {
		return finalState;
	}

	public Map<Integer, List<Transition>> getTrans() {
		return trans;
	}

	public void addTrans(Integer s1, String lab, Integer s2) {
		List<Transition> s1Trans;
		if (trans.containsKey(s1))
			s1Trans = trans.get(s1);
		else {
			s1Trans = new LinkedList<Transition>();
			trans.put(s1, s1Trans);
		}
		if (!trans.containsKey(s2))
			trans.put(s2, new LinkedList<Transition>());

		s1Trans.add(new Transition(lab, s2));
	}

	public void addTrans(Map.Entry<Integer, List<Transition>> tr) {
		trans.put(tr.getKey(), tr.getValue());
	}

	public List<String> getLabels() {
		List<String> labels = new ArrayList<>();
		for (Map.Entry<Integer, List<Transition>> entry : this.getTrans().entrySet()) {
			for (Transition t : entry.getValue()) {
				if (!(t.lab.equals("epsilon")) && (!labels.contains(t.lab)))
					labels.add(t.lab);
			}
		}
		return labels;
	}
	

	public Set<Integer> getStates() {
		return this.getTrans().keySet();
	}

	public void resetStates() {
		start=1;
		accepting=2;
	}
	public String toString() {
		TreeMap<Integer, List<Transition>> sorted = new TreeMap<>(trans);
		return "NFA:" + "\n" + "start->" + startState + " | final: ((" + finalState + "))" + "\n" + "Transitions: "
				+ sorted;
	}

	// ----------------automata for basis
	// cases----------------------------------------------------------
	static Nfa automatonfor0() {
		return new Nfa();
	}
	static Nfa automatofor0(Integer startState, Integer finalState) {
		return new Nfa(startState, finalState);
	}
	
	static Nfa automatonforEpsilon() {
		Nfa n = new Nfa();
		n.addTrans(n.getStart(), "epsilon", n.getFinal());
		return n;
	}
	static Nfa automatoforEpsilon(Integer startState, Integer finalState) {
		Nfa n = new Nfa(startState, finalState);
		n.addTrans(n.getStart(), "epsilon", n.getFinal());
		return n;
	}

	static Nfa automatonForSymbol(String label) {
		Nfa n = new Nfa();
		n.addTrans(n.getStart(), label, n.getFinal());
		return n;
	}
	
	static Nfa automatonForSymbol(String label, Integer startState, Integer finalState) {
		Nfa n = new Nfa(startState, finalState);
		n.addTrans(n.getStart(), label, n.getFinal());
		return n;
	}

	// -------------------Operators--------------------------------------------------------
	// Union (a|b) Transitions: case 1 in Aho-Ullman Induction
	static Nfa union(Nfa aut1, Nfa aut2) {
		Nfa n = new Nfa(aut1.getStart() - 1, aut2.getFinal() + 1, aut1.getTrans());
		//System.out.println("aut1 transitions="+aut1.getTrans());
		//System.out.println("aut2 transitions="+aut2.getTrans());
		n.getTrans().putAll(aut2.getTrans());

		// new start state
		n.addTrans(n.getStart(), "epsilon", aut1.getStart());
		n.addTrans(n.getStart(), "epsilon", aut2.getStart());

		// new accepting state
		n.addTrans(aut1.getFinal(), "epsilon", n.getFinal());
		n.addTrans(aut2.getFinal(), "epsilon", n.getFinal());

		return n;
	}

	// Concatenation (ab) Transitions :case 2 in Aho-Ullman Induction
	static Nfa concatenation(Nfa aut1, Nfa aut2) {
		Nfa n = new Nfa(aut1.getStart(), aut2.getFinal(), aut1.getTrans());
		n.getTrans().putAll(aut2.getTrans());

		// adding epsilon transition from final state of aut1 to start state of aut2
		n.addTrans(aut1.getFinal(), "epsilon", aut2.getStart());
		return n;

	}

	// closure (c*) :case 3 in Aho-Ullman Induction
	static Nfa closure(Nfa aut) {
		Nfa n = new Nfa(aut.getStart() - 1, aut.getFinal() + 1, aut.getTrans());
		// si le symbol est pr�sent 0 fois on veut pouvoir passer directement dans
		// l'�tat final
		n.addTrans(n.getStart(), "epsilon", n.getFinal());

		// transition epsilon du nouvel �tat initial vers l'ancien �tat initial
		n.addTrans(n.getStart(), "epsilon", aut.getStart());

		// transition epsilon de l'ancien �tat final vers l'ancien �tat initial
		n.addTrans(aut.getFinal(), "epsilon", aut.getStart());

		// transition epsilon de l'ancien �tat final vers le nouvel �tat final
		n.addTrans(aut.getFinal(), "epsilon", n.getFinal());
		return n;

	}
	
	// Operator +:  (c+) 
		static Nfa plusOperator(Nfa aut) {
			Nfa n = new Nfa(aut.getStart() - 1, aut.getFinal() + 1, aut.getTrans());
			// transition epsilon du nouvel �tat initial vers l'ancien �tat initial
			n.addTrans(n.getStart(), "epsilon", aut.getStart());

			// transition epsilon de l'ancien �tat final vers l'ancien �tat initial
			n.addTrans(aut.getFinal(), "epsilon", aut.getStart());

			// transition epsilon de l'ancien �tat final vers le nouvel �tat final
			n.addTrans(aut.getFinal(), "epsilon", n.getFinal());
			return n;
		}
	
	// list of states accessible from a specific state with epsilon transitions
	static Set<Integer> getEpsilonClosure(Integer state, Nfa n,Set<Integer> result) {
		//Set<Integer> result = new HashSet<>();
		//System.out.println("state= "+state);
		result.add(state);
		List<Transition> transitions = n.getTrans().get(state);
		for (Transition t : transitions) {
			if (t.lab.equals("epsilon")) {
				//�viter les boucles infinis quand 2 �tats ont des transitions epsilons de chaque c�t�
				//System.out.println("result= "+result.toString());
				//System.out.println("result contains "+t.target +" ? "+result.contains(t.target));
				if(!result.contains(t.target))
					result.addAll(getEpsilonClosure(t.target, n,result));
				else break;
			}
		}
		return result;
	}

	static Set<Integer> getSymbolClosure(String label, Integer state, Nfa n,Set<Integer> result) {
		
		List<Transition> transitions = n.getTrans().get(state);
		for (Transition t : transitions) {
			if (t.lab.equals(label)) {
				if(!result.contains(t.target)) {
					result.add(t.target);
					result.addAll(getSymbolClosure(label, t.target, n,result));
				}
				else break;	
			}
		}
		return result;
	}

	static Set<Integer> unionTransitions(Set<Integer> states, Nfa n, String label) {
		Set<Integer> result = new HashSet<Integer>();
		for (Integer state : states) {
			result.addAll(getSymbolClosure(label, state, n,result));
		}
		return result;
	}

	public static boolean isFinal(Set<Integer> stateDfa, Nfa n) {
		for (Integer state : stateDfa) {
			if (n.getFinal().equals(state))
				return true;
		}
		return false;
	}

	// non deterministic automaton to equivalent deterministic automaton using the Subset Construction
	public  Dfa toDfa(Nfa n) {
		List<String> labels = n.getLabels();
		

		int numfinalstates = 0;
		System.out.println("n start: "+n.getStart());
		Set<Integer> result = new HashSet<>();
		Set<Integer> initialState = getEpsilonClosure(n.getStart(),n, result);
		List<Set<Integer>> finalstatesdfa = new ArrayList<>();
		List<Set<Integer>> statesdfa = new ArrayList<>();
		statesdfa.add(initialState);
		if (isFinal(initialState, n))
			numfinalstates++;

		List<TransitionDfa> transitions = new ArrayList<>();
		int computedRows = 0;
		Set<Integer> result2;
		while (statesdfa.size() > computedRows) {
			for (String label : labels) {
				result2=new HashSet<>();
				Set<Integer> uniontrans = unionTransitions(statesdfa.get(computedRows), n, label);
				for (Integer s : uniontrans) {
					uniontrans.addAll(getSymbolClosure("epsilon", s, n,result2));
				}
				if (!uniontrans.isEmpty()) {
					if (!statesdfa.contains(uniontrans)) {
						statesdfa.add(uniontrans);
						if (isFinal(uniontrans, n))
							numfinalstates++;
					}
					TransitionDfa t = new TransitionDfa(label, statesdfa.get(computedRows), uniontrans);
					transitions.add(t);
					//System.out.println("ajout d'une transition dfa : "+ statesdfa.get(computedRows) +" -> "+ uniontrans);
				}

			}
			computedRows++;
		}
		Dfa d = new Dfa(statesdfa.size(), numfinalstates);

		for (Set<Integer> state : statesdfa) {
			if (state.contains(n.finalState)) {
				d.addFinal(statesdfa.indexOf(state));
				finalstatesdfa.add(state);
			}

		}

		for (TransitionDfa tdfa : transitions) {
			d.addTrans(statesdfa.indexOf(tdfa.from), tdfa.lab.charAt(0), statesdfa.indexOf(tdfa.target));
		}
		System.out.println("DFA:" + "\n" + "start->" + statesdfa.get(0).toString() + " | finals: ((" + finalstatesdfa.toString() + "))" + "\n" + "Transitions: "
		+ transitions.toString()) ;

		return d;
	}
	
}
