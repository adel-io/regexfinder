package utilitaires;
import java.util.Set;

public class TransitionDfa {
	String lab;
	Set<Integer> from;
	Set<Integer> target;

	public TransitionDfa(String lab,Set<Integer> from, Set<Integer> target) {
		this.lab = lab;
		this.from = from;
		this.target = target;
	}

	public String toString() {
		return "" +from+ "->"+ lab + "->" + target;
	}
}
