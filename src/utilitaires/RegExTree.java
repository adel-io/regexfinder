package utilitaires;
import java.util.ArrayList;

public class RegExTree {
	
		protected int root;
		protected ArrayList<RegExTree> subTrees;
		static final int asciiMax = 255;

		public RegExTree(int root, ArrayList<RegExTree> subTrees) {
			this.root = root;
			this.subTrees = subTrees;
		}
		// FROM TREE TO PARENTHESIS
		public String toString() {
			if (subTrees.isEmpty())
				return rootToString();
			// RegExTree r=subTrees.get(0);
			// r.rootToString()
			String result = rootToString() + "(" + subTrees.get(0).toString();
			for (int i = 1; i < subTrees.size(); i++)
				result += "," + subTrees.get(i).toString();
			return result + ")";
		}
		public String postOrder() {
			if (subTrees.isEmpty())
				return rootToString();

			if ((root == RegEx.ETOILE) || (root == RegEx.PLUS))
				return subTrees.get(0).postOrder() + "->" + rootToString();

			return subTrees.get(0).postOrder() + "->" + subTrees.get(1).postOrder() + "->" + rootToString();

		}
		public boolean isLeaf() {
			return subTrees.isEmpty();
		}

		private String rootToString() {
			if (root == RegEx.CONCAT)
				return ".";
			if (root == RegEx.ETOILE)
				return "*";
			if (root == RegEx.PLUS)
				return "+";
			if (root == RegEx.ALTERN)
				return "|";
			if (root == RegEx.DOT)
				return ".";
			return Character.toString((char) root);
		}
		
		// RegExTree to NFA
		public Nfa toNfa(RegExTree r) {
			if (r.subTrees.isEmpty()) {
				return Nfa.automatonForSymbol(r.rootToString());
			}
			if (r.root == RegEx.ALTERN) {
				return Nfa.union(toNfa(r.subTrees.get(0)), toNfa(r.subTrees.get(1)));
			}

			if (r.root == RegEx.CONCAT) {
				return Nfa.concatenation(toNfa(r.subTrees.get(0)), toNfa(r.subTrees.get(1)));
			}
			if (r.root == RegEx.ETOILE ) {
				return Nfa.closure(toNfa(r.subTrees.get(0)));
			}
			if (r.root == RegEx.PLUS ) {
				return Nfa.plusOperator(toNfa(r.subTrees.get(0)));
			}
		
			if (r.root == RegEx.DOT)
				return Nfa.automatonforEpsilon();
			
			return toNfa(r.subTrees.get(0));
		}

}
